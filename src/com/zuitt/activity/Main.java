package com.zuitt.activity;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        //STEP 9
        Phonebook phonebook = new Phonebook();

        //STEP 10
        Contact contact1 = new Contact();
        contact1.setName("John Doe");
        contact1.setContactNumber("+639152468596");
        contact1.setAddress("Quezon City");

        Contact contact2 = new Contact();
        contact2.setName("Jane Doe");
        contact2.setContactNumber("+639162148573");
        contact2.setAddress("Caloocan City");

        //STEP 11
        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        //STEP 12
        if(phonebook.getContacts().size() == 0 ){
            System.out.println("Phonebook is empty");
        }else{
            phonebook.getContacts().forEach(contact -> {
                System.out.println("---------------------------");
                System.out.println(contact.name);
                System.out.println("---------------------------");
                System.out.println(contact.name + " has the following registered number:");
                System.out.println(contact.contactNumber);
                System.out.println(contact.name + " has the following registered address:");
                System.out.println("My home in " + contact.address);
            });
        }
    }
}
