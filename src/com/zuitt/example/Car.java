package com.zuitt.example;

public class Car {
    //1.) Properties - characteristics of an object
        //If no keyword, default is accessible within the whole package
        //protected - accessible by the class of same package and subclass present in any package
        //public - can be accessed anywhere
        private String name; // property/method accessible within the class only
        private String brand;
        private int yearOfMake;
        //Make a component of a car
        private  Driver driver;

    //2.) Constructors - used to create/instantiate an object:
        //Empty Constructor - creates object that doesn't have any arguments/parameters
        public Car(){
            //whenever a new car is created,it sets driver to default: "Alejandro"
            this.driver = new Driver("Alejandro");
        }

        //Parameterized Constructor - creates an object with argument/parameters.
        public Car(String name, String brand, int yearOfMake){
            this.name = name;
            this.brand = brand;
            this.yearOfMake = yearOfMake;
            this.driver = new Driver("Alejandro");
        }
    //3.) Getters and Setters - get and set the values of each property of the object.
        //Getters - used to retrieve the value of instantiatedd object.
        public  String getName(){
                return this.name;
        }
        public  String getBrand(){
            return this.brand;
        }
        public  int getYearOfMake(){
            return this.yearOfMake;
        }

        public String getDriverName(){
            return this.driver.getName();
        }

        //Setters - use to change the default values of an instantiated object.
        public  void setName(String name){
            this.name = name;
        }
        public  void setBrand(String brand){
            this.brand = brand;
        }
        public  void setYearOfMake(int yearOfMake){
            if (yearOfMake < 2022){
                this.yearOfMake = yearOfMake;
            }
        }
        public void setDriverName(String driver){
            this.driver.setName(driver);
        }
    //4.) Methods - functions that an object can perform (actions)
        public  void drive(){
            System.out.println("The car is running.");
        }
}
