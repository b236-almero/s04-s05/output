package com.zuitt.example;

//Child Class of Animal Class
//"extend" keyword is used to inherit the properties and methods of the parent class
//cannot "extend" multiple classes
public class Dog extends Animal{

    //Properties
    private String breed;

    //Constructor
    public Dog(){
        //This will inherit the Animal() constructor.
        super();
        //sets breed as default "chihuahua"
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    //Getter and Setter
    public  String getBreed(){
        return  this.breed;
    }
    public void setBreed(String breed){
        this.breed = breed;
    }

    //Methods
    public void speak(){
        System.out.println("Woof Woof");
    }
}
