package com.zuitt.example;

//Interfaces Implementation
public class Person implements  Actions, Greetings{

    //Actions -------------
    public void sleep(){
        System.out.println("Zzzz...");
    }

    public void run(){
        System.out.println("Running...");
    }

    //Greetings -------------
    public void morningGreet() {
        System.out.println("Good morning!");
    }

    public void holidayGreet() {
        System.out.println("Happy Holidays!");
    }
}
